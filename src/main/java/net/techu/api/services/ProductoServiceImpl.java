package net.techu.api.services;

import net.techu.api.models.Producto;
import net.techu.api.repository.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.stream.Collectors;

@Service("productoService")
//@Transactional

public class ProductoServiceImpl implements ProductoService{

    private ProductoRepository productoRepository;
    @Autowired
    public ProductoServiceImpl(ProductoRepository productoRepository)
    {
        this.productoRepository = productoRepository;
    }

    @Override
    public List<Producto> findAll() {
        return productoRepository.findAll();
    }

    @Override
    public Producto saveProducto(Producto p) {

        return productoRepository.saveProducto(p);
    }

    @Override
    public void deleteProducto(String codigo) {
        productoRepository.deleteProducto(codigo);
    }

    @Override
    public void updateProducto(Producto p) {
        productoRepository.updateProducto(p);
    }

    @Override
    public Producto findOne(String codigo) {
        return productoRepository.findOne(codigo);
    }
}
