package net.techu.api.controllers;

import net.techu.api.models.Producto;
import net.techu.api.services.ProductoService;
import net.techu.api.services.ProductoServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
public class ProductoController {


    private final ProductoService productoService;
    @Autowired
    public ProductoController(ProductoService productoService)
    {
        this.productoService = productoService;

    }

    @RequestMapping(value = "/productos", method = RequestMethod.GET)
    public ResponseEntity<Object> getListaProductos() {


            return new ResponseEntity<Object>(productoService.findAll(), HttpStatus.OK);

    }

    @RequestMapping(value = "/productos/{id}", method = RequestMethod.GET)
    public ResponseEntity<Object> getProducto(@PathVariable("id") String id) {
        Producto p = productoService.findOne(id);
        if (p != null) {
            return new ResponseEntity<Object>(productoService.findOne(id), HttpStatus.OK);

        }
        return new ResponseEntity<Object>("No se encontro producto: " + id, HttpStatus.NOT_FOUND);
    }

    @PostMapping(value="/productos")
    public ResponseEntity<Object> addProducto(@RequestBody Producto productoNuevo) {
        Producto p = productoService.saveProducto(productoNuevo);
        if (p == null) {
            return new ResponseEntity<Object>("Producto ya existe", HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<Object>("Producto "+p.getId()+" creado", HttpStatus.CREATED);
    }

    @PutMapping(value="/productos/{id}")
    public ResponseEntity<Object> updateProducto(@PathVariable("id") String id, @RequestBody Producto productoModificado) {
        if(productoModificado.getId()==null){
            productoModificado.setId(id);
        }
        if(id.toUpperCase().equals(productoModificado.getId().toUpperCase()) ) {
            Producto p = productoService.findOne(id);
            if (p != null) {
                productoService.updateProducto(productoModificado);

                return new ResponseEntity<Object>("Producto " + id + " actualizado", HttpStatus.OK);
            }
            else{
                return new ResponseEntity<Object>("No se encontro producto: "+id, HttpStatus.NOT_FOUND);
            }
        }
        else {
            return new ResponseEntity<Object>("Datos incorrectos", HttpStatus.CONFLICT);
        }

    }

    @DeleteMapping(value="/productos/{id}")
    public ResponseEntity<Object> deleteProducto(@PathVariable("id") String id) {
        Producto p = productoService.findOne(id);
        if (p != null) {
            productoService.deleteProducto(id);

            return new ResponseEntity<Object>("Producto "+id+" eliminado", HttpStatus.OK);
        }
        return new ResponseEntity<Object>("No se encontro producto: "+id, HttpStatus.NOT_FOUND);
    }
}
