package net.techu.api.repository;

import net.techu.api.models.Producto;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
public class ProductoRepositoryImpl implements ProductoRepository{
    private static Map<String, Producto> listaProductos = new HashMap<>();

    static {
        Producto prd1 = new Producto("PR1", "PRODUCTO1", 1);
        listaProductos.put(prd1.getId(), prd1);
        Producto prd2 = new Producto("PR2", "PRODUCTO2", 1);
        listaProductos.put(prd2.getId(), prd2);
    }



    @Override
    public List<Producto> findAll() {
        List<Producto> listado = listaProductos.values().stream().collect(Collectors.toList());
        return listado;
    }

    @Override
    public Producto saveProducto(Producto p) {

        if (listaProductos.containsKey(p.getId().toUpperCase())) {

            return null;
        }
        listaProductos.put(p.getId(),p);
        return p;
    }

    @Override
    public void deleteProducto(String codigo) {
        listaProductos.remove(codigo.toUpperCase());
    }

    @Override
    public void updateProducto(Producto p) {
        listaProductos.put(p.getId().toUpperCase(), p);
    }

    @Override
    public Producto findOne(String codigo) {
        if (listaProductos.containsKey(codigo.toUpperCase())) {
            return listaProductos.get(codigo.toUpperCase());
        }
        return null;
    }
}
