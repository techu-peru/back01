package net.techu.api.repository;

import net.techu.api.models.Producto;

import java.util.List;

public interface ProductoRepository {
    List<Producto> findAll();
    public Producto saveProducto(Producto p);
    public void deleteProducto (String codigo);
    public void updateProducto(Producto p);
    public Producto findOne(String codigo);
}
